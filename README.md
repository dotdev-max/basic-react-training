# Basic React Training

Use this repo to get started with the React training. Use this readme as a reference guide and feel free to come back to it (or find me and ask me questions).

## Goal

You're creating the barebones of an online store, all using React. The most basic components of a shop is a list of products (PLP) and a shopping cart. 
We can of course add as much as we want to that list, like PDP and different pages, as well as a checkout (and we'll get to that later), 
but the PLP and shopping cart are enough for now.

## Tips

- I have already imported the `Shop` component in `App.js` so you don't need to worry about that. 
However, any other components you write will need to be imported where they are used.
- I have also created all the files you should need to do the bare minimum to get this application working.
- Use the `Shop` component as the base for your other code. That means, import and use the `PLP` and `Cart` components there.
- Prefer functional components over class components (but feel free to mix it up if you want, it's good to learn both)
- To dynamically generate a list of elements in react (for example `<li>` tags), use a map function since it returns an array. 
You can then add that array in the jsx by including it inside of brackets (`{listOfElements}`)

## Acceptance Criteria

- You should have a list of products
- You should be able to add the products to the shopping cart
- You should be able to remove products from the shopping cart
- When a product is added to the shopping cart it should be removed from the PLP (don't worry about any other inventory)
- You should use all the provided files in the `Shop` folder (feel free to create more if you want)

## Bonus

- Feel free to do some styling. 
- Feel free to build on this base and make it more advanced (for example add up the total cost of the items in the shopping cart)

Good luck! Let me know if you need anything else.